<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class LampadasTest extends TestCase
{
    public function testGetStatusLampadas(){
        $c=new Controller;
        $lampadas=$c->corrida(3);

        //Checa se o objeto é do tipo array
        $this->assertInternalType('array', $lampadas);

        //Checa se o tamanho do array é igual a 3
        $this->assertEquals(3, count($lampadas));

        //Checa se o status das lampadas bate com o teste
        $this->assertEquals('On Off Off', $c->statusLampadas());
    }
}
