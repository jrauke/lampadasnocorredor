<?php

namespace App\Lampadas;

class Controller {

    public function corrida($qtdLampadas) {

        $lampadas = [];
        for($i = 0; $i < $qtdLampadas; $i++) {
			$lampada = new Lampada;
            $lampadas[$i] = $lampada;
        }

        $lampadas = $this->caminhar($lampadas);

        $this->statusLampadas($lampadas);
    }

    public function caminhar($lampadas) {
        
        $n = count($lampadas);
        
        for ($i = 0; $i < $n; $i++) {
            $j = 0;
            foreach($lampadas as $lampada) {
                if (($j + 1) % ($i + 1) == 0) {
                    $lampadas[$j] = $this->apertarInterruptor($lampadas[$j]);
                }
                $j++;
            }
        }
        return $lampadas;
    }
    
    public function apertarInterruptor($lampada) {
        $lampada->setLigada(!$lampada->getLigada());
        return $lampada;
    }
    
    public function statusLampadas($lampadas) {
        $view = new View;

		foreach ($lampadas as $lampada) {
            if($lampada->getLigada()) {
                $view->render("\nOn");
            } else {
                $view->render("\nOff");
            }
			
		}
	}
}