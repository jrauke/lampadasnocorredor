<?php

namespace App\Lampadas;

class Lampada {
    
    private $ligada = false;

    public function setLigada($ligada) {
        $this->ligada = $ligada;
    }

    public function getLigada() {
        return $this->ligada;
    }
}